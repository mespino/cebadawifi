#ifndef Sensors_h
#define Sensors_h

#include "Arduino.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#include <HX711.h>

class Sensors
{
  private:
  
    OneWire oneWire;
    DallasTemperature ds_sensor;
    HX711 scale;
    //byte allAddress [4][8];
    //unsigned char totalDevices;
    //const unsigned char maxNumberOfDevices;
    const unsigned char resolution; //bits
    
  public:
   Sensors(int ONE_WIRE_BUS, int HX711_DOUT, int HX711_PD_SCK);
   void begin();
   void requestTemperature();
   float getTemperature();
   byte checkSensor();
   float getDensity();
};

#endif
