//Cebada Wifi
//Controlador de temperatura de fermentación y medidor en línea de densidad

#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <HX711.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include "Control.h"
#include "Display.h"
#include "System.h"


//Constant definition
#define thingspeakUpdatePeriod 30 //in sec | Thingspeak pub update rate
#define tempProcessingPeriod 5  //Period for temperature control
#define densityUpdatePeriod 5 //Period for density update


//IO pin definition
#define s_ONE_WIRE_BUS 2  //DS18B20 on arduino pin2 corresponds to D4 on physical board
#define s_HX711_DOUT 12 //HX711 DOUT pin
#define s_HX711_PD_SCK 14 //HX711 Serial clock
#define s_OUT_HEAT  5
#define s_OUT_COOL  10

  
//Object definition
//HX711 scale(HX711_DOUT, HX711_PD_SCK);  // Scale definition
//OneWire oneWire(ONE_WIRE_BUS);  // One-wire bus definition
//DallasTemperature DS18B20(&oneWire);  // DS18B20 temperature sensor definition
ESP8266WebServer webServer(80);  //Web Server definition on port 80
System cebadasystem(
            s_ONE_WIRE_BUS,
            s_OUT_HEAT,
            s_OUT_COOL,
            s_HX711_DOUT,
            s_HX711_PD_SCK
);

//Global variables definition
float prevTemp = 0;
const char* apiServer = "api.thingspeak.com";
String apiKey = "ZHH6FOKYU95E0JYU";
char* MY_SSID[] = {"testWiFi","MLCCWLESS-CORP","MLCC-CORP"}; 
char* MY_PWD[] = {"testPWD","15062012caserones","15062012caserones"};
unsigned long tempProcessingTime, densityUpdateTime, thingspeakUpdateTime;
float temp, density, setpoint;
String webSite,javaScript,XML,logTxt;


void setup(){
    bool conn_status;
  int i;
  Serial.begin(115200);
  delay(5000);
  for(i=0; i<sizeof(*MY_SSID); i++){
    if(connectWifi(MY_SSID[i],MY_PWD[i])){
      conn_status = true;
      break;
    }
  }
  if (conn_status == false){
    WiFiManager wifiManager;
    wifiManager.setTimeout(120);
    //connectWifi();
    if(!wifiManager.autoConnect("AP-ESP")) {
      Serial.println("failed to connect and hit timeout");
      delay(3000);
      //reset and try again, or maybe put it to deep sleep
      ESP.reset();
      delay(5000);
    } 
  }
    printLogln("connected!");
  printLog("IP address: ");
  Serial.println(WiFi.localIP());
  printLogln(String(WiFi.localIP()));
  printLogln("DS18B20-HX711 Demo");
  webServer.begin();
  webServer.on("/",handleWebsite);
  webServer.on("/xml",handleXML);
  cebadasystem.begin();
}


void loop(){
  if ((millis() - tempProcessingTime > 1000*tempProcessingPeriod) || (millis() - tempProcessingTime < 0)) {
    tempProcessingTime = millis();
    temp = cebadasystem.temp_processing();
    printLog(String(millis()/1000));
    printLog("Temperature: \t\t");
    printLogln(String(temp));
  }
  if ((millis() - densityUpdateTime > 1000*densityUpdatePeriod) || (millis() - densityUpdateTime < 0)) {   
    densityUpdateTime = millis();
    density = cebadasystem.density_update();
    printLog("Density: \t\t");
    printLogln(String(density));
  } 
  if ((millis() - thingspeakUpdateTime > 1000*thingspeakUpdatePeriod) || (millis() - thingspeakUpdateTime < 0)) {
    thingspeakUpdateTime = millis();
    updateThingspeak(temp, density);  
  }
  webServer.handleClient();
} 


bool connectWifi(char* ssid, char* pwd){
  int numretries = 0;
  int maxretries = 10;
  Serial.print("Connecting to "+*ssid);
  WiFi.begin(ssid, pwd);
  while ((WiFi.status() != WL_CONNECTED) && (numretries < maxretries)){
    delay(1000);
    Serial.print(".");
    numretries++;
  }
  if (WiFi.status() == WL_CONNECTED){
    Serial.println("Connected!");
    return true;
  } else{
    Serial.println("Connection failed");  
    return false;
  }
}


void updateThingspeak(float temp, float weight){  
  WiFiClient client;
  if (client.connect(apiServer, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
    printLog("WiFi Client connected, sending data to thingspeak ");
    String postStr = apiKey;
    postStr += "&field1=";  
    postStr += String(temp);
    postStr += "&field2=";  
    postStr += String(weight);
    postStr += "\r\n\r\n";
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);
    delay(500);
  }//end if
  client.stop();
}//end send


void InitScale(HX711 scale){
  Serial.println("Before setting up the scale:");
  Serial.print("read: \t\t");
  Serial.println(scale.read());           // print a raw reading from the ADC
  Serial.print("read average: \t\t");
  Serial.println(scale.read_average(20)); // print the average of 20 readings from the ADC
  Serial.print("get value: \t\t");
  Serial.println(scale.get_value(5));     // print the average of 5 readings from the ADC minus the tare weight (not set yet)
  Serial.print("get units: \t\t");
  Serial.println(scale.get_units(5), 1);  // print the average of 5 readings from the ADC minus tare weight (not set) divided 
                                          // by the SCALE parameter (not set yet)  
  scale.set_scale(2158.45f);              // this value is obtained by calibrating the scale with known weights; see the README for details
  //scale.set_scale();
  scale.tare();                           // reset the scale to 0
  Serial.println("After setting up the scale:");
  Serial.print("read: \t\t");
  Serial.println(scale.read());           // print a raw reading from the ADC
  Serial.print("read average: \t\t");
  Serial.println(scale.read_average(20)); // print the average of 20 readings from the ADC
  Serial.print("get value: \t\t");
  Serial.println(scale.get_value(5));     // print the average of 5 readings from the ADC minus the tare weight, set with tare()
  Serial.print("get units: \t\t");
  Serial.println(scale.get_units(5), 1);  // print the average of 5 readings from the ADC minus tare weight, divided 
                                          // by the SCALE parameter set with set_scale
  Serial.println("Readings:");
}


void buildWebsite(){
  buildJavascript();
  webSite="<!DOCTYPE HTML>\n";
  webSite+="<head>\n  <meta charset=\"utf-8\">\n  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n";
  webSite+="  <title>Cebada</title>\n";
  webSite+="  <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n";
  webSite+="  <style>\n";
  webSite+="    .glyphicon.glyphicon-circle-red:before { content: \"\\25cf\"; font-size: 1.5em; color:red;}\n";
  webSite+="    .glyphicon.glyphicon-circle-black:before { content: \"\\25cf\"; font-size: 1.5em;}\n";
  webSite+="  </style>\n";
  webSite+=javaScript;
  webSite+="<BODY onload='process()'>\n";
  webSite+="<div class=\"container\">\n";
  webSite+="  <nav class=\"navbar navbar-default\">\n    <div class=\"container-fluid\">\n      <div class=\"navbar-header\">\n";
  webSite+="        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n";
  webSite+="          <span class=\"sr-only\">Toggle navigation</span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n";
  webSite+="        </button>\n        <a class=\"navbar-brand\" href=\"#\">Cebada</a>\n";
  webSite+="      </div>\n";
  webSite+="      <div id=\"navbar\" class=\"navbar-collapse collapse\">\n";
  webSite+="        <ul class=\"nav navbar-nav\">\n";
  webSite+="          <li id=\"homeLink\" class=\"active\"><a href=\"#\" onClick=\"return showHome();\">Home</a></li>\n";
  webSite+="          <li id=\"logLink\"><a href=\"#\" onClick=\"return showLog();\">Log</a></li>\n";
  webSite+="        </ul>\n";
  webSite+="      </div>\n    </div>\n  </nav>\n";
  webSite+="  <div id=\"homeScreen\" class=\"panel panel-default\">\n";
  webSite+="    <div class=\"panel-body\">\n";
  webSite+="      <div class=\"row\">\n";
  webSite+="        <div class=\"col-xs-4 col-md-2 col-lg-1\">\n";
  webSite+="          <p>Heat <span id='heaticon' class=\"glyphicon glyphicon-circle-red\"></span></p>\n";
  webSite+="          <p>Cool <span id='coolicon' class=\"glyphicon glyphicon-circle-black\"></span></p>\n";
  webSite+="        </div>\n";
  webSite+="        <div class=\"col-xs-8 col-md-10 col-lg-11\">\n";
  webSite+="          <h1 id='tempvalue'></h1>\n";
  webSite+="          <h2 id='SPvalue'></h2>\n";
  webSite+="        </div>\n";
  webSite+="      </div>\n"; 
  webSite+="      <p>Weight = <a id='weightvalue'></a></p>\n";
  webSite+="    </div>\n";
  webSite+="    <div id='footer' class=\"panel-footer\" align=\"right\">(C)2016</div>";
  webSite+="  </div>\n";   
  webSite+="  <div id=\"logScreen\" class=\"panel panel-default\" onload='$('#logScreen').hide()'>\n";
  webSite+="    <div id=\"logScreenTxt\" class=\"panel-body\"></div>\n";
  webSite+="  </div>\n";
  webSite+="</div>\n";
  webSite+="<script src=\"https://code.jquery.com/jquery-3.1.0.min.js\" integrity=\"sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=\" crossorigin=\"anonymous\"></script>\n"; 
  webSite+="<script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n";
  webSite+="</BODY>\n</HTML>";
  //webSite+="      <iframe width=\"450\" height=\"260\" style=\"border: 1px solid #cccccc;\" src=\"https://thingspeak.com/channels/160465/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=100&type=line\"></iframe>\n";
}

void buildJavascript(){
  javaScript="<SCRIPT>\n";
  javaScript+="var xmlHttp=createXmlHttpObject();\n";
  javaScript+="function createXmlHttpObject(){\n";
  javaScript+=" if(window.XMLHttpRequest){\n";
  javaScript+="    xmlHttp=new XMLHttpRequest();\n";
  javaScript+=" }else{\n";
  javaScript+="    xmlHttp=new ActiveXObject('Microsoft.XMLHTTP');\n";
  javaScript+=" }\n";
  javaScript+=" return xmlHttp;\n";
  javaScript+="}\n";
  
  javaScript+="function process(){\n";
  javaScript+=" if(xmlHttp.readyState==0 || xmlHttp.readyState==4){\n";
  javaScript+="   xmlHttp.open('PUT','xml',true);\n";
  javaScript+="   xmlHttp.onreadystatechange=handleServerResponse;\n";
  javaScript+="   xmlHttp.send(null);\n";
  javaScript+=" }\n";
  javaScript+=" setTimeout('process()',1000);\n";
  javaScript+="}\n";
  
  javaScript+="function handleServerResponse(){\n";
  javaScript+=" if(xmlHttp.readyState==4 && xmlHttp.status==200){\n";
  javaScript+="   xmlResponse=xmlHttp.responseXML;\n";
  javaScript+="   xmldoc = xmlResponse.getElementsByTagName('tempvalue');\n";
  javaScript+="   tempmessage = xmldoc[0].firstChild.nodeValue;\n";
  javaScript+="   xmldoc = xmlResponse.getElementsByTagName('SPvalue');\n";
  javaScript+="   setpointmessage = xmldoc[0].firstChild.nodeValue;\n";
  javaScript+="   xmldoc = xmlResponse.getElementsByTagName('weightvalue');\n";
  javaScript+="   weightmessage = xmldoc[0].firstChild.nodeValue;\n";
  javaScript+="   xmldoc = xmlResponse.getElementsByTagName('footer');\n";
  javaScript+="   footermessage = xmldoc[0].firstChild.nodeValue;\n";
  javaScript+="   xmldoc = xmlResponse.getElementsByTagName('logTxt');\n";
  javaScript+="   logmessage = xmldoc[0].firstChild.nodeValue;\n";
  javaScript+="   document.getElementById('tempvalue').innerHTML= \"<small>PV: </small>\" + tempmessage + \"<small> °C</small>\";\n";
  javaScript+="   document.getElementById('SPvalue').innerHTML= \"<small>SP: </small>\" + setpointmessage + \"<small> °C</small>\";\n";
  javaScript+="   document.getElementById('weightvalue').innerHTML=weightmessage;\n";
  javaScript+="   document.getElementById('footer').innerHTML=footermessage;\n";
  javaScript+="   document.getElementById('logScreenTxt').innerHTML+=logmessage;\n";
  javaScript+=" }\n";
  javaScript+="}\n";

  javaScript+="function showHome(){ $('#homeScreen').show(); $('#homeLink').addClass(\"active\"); $('#logScreen').hide(); $('#logLink').removeClass(\"active\");}\n";
  javaScript+="function showLog(){ $('#homeScreen').hide(); $('#homeLink').removeClass(\"active\"); $('#logScreen').show(); $('#logLink').addClass(\"active\"); }\n";
  javaScript+="</SCRIPT>\n";
}

void buildXML(){
  XML="<?xml version='1.0'?>";
  XML+="<response>\n";
  XML+="<tempvalue>";
  XML+=String(temp);
  XML+="</tempvalue>\n<SPvalue>\n";
  XML+=String(setpoint);
  XML+="</SPvalue>\n<weightvalue>";
  XML+=String(density);
  XML+="</weightvalue>\n<footer>";
  XML+="CPU running time: "+ millis2time() + "    |    IP Address: " + (String)WiFi.localIP() + "    |    (C)2016"; 
  XML+="</footer>\n<logTxt>\n";
  XML+=logTxt;
  XML+="</logTxt>";
  XML+="</response>"; 
}


String millis2time(){
  String Time="";
  unsigned long ss;
  byte mm,hh;
  ss=millis()/1000;
  hh=ss/3600;
  mm=(ss-hh*3600)/60;
  ss=(ss-hh*3600)-mm*60;
  if(hh<10)Time+="0";
  Time+=(String)hh+":";
  if(mm<10)Time+="0";
  Time+=(String)mm+":";
  if(ss<10)Time+="0";
  Time+=(String)ss;
  return Time;
}


void handleWebsite(){
  buildWebsite();
  webServer.send(200,"text/html",webSite);
}


void handleXML(){
  buildXML();
  webServer.send(200,"text/xml",XML);
  logTxt= "";
}


void printLog(String message){
  Serial.print(message);
  logTxt += message;
}


void printLogln(String message){
  Serial.println(message);
  logTxt += message;
}
