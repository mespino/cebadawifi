#ifndef System_h
#define System_h

#include "Arduino.h"
#include "Control.h"
#include "Sensors.h"
#include "Display.h"

class System
{
  private:
    /***datos volatiles***/
    Control *ControlCore; //Includes all control algorithms
    Sensors sensors;
    //Display hd_display; No display so far
    unsigned char system_mode;
    //unsigned char must_save;
    unsigned char current_option;
    unsigned char current_param_value;
    unsigned long last_display_loop_time; //No display so far
    unsigned long last_temp_processing_time;
    
    int pin_OUT_HEAT;
    int pin_OUT_COOL;   
    int pin_HX711_DOUT;
    int pin_HX711_PD_SCK;
    
    void updateStatus();
    void updateOutputs();
    void updateTemp();
    
  public:
  
    enum Opt {UP, DOWN, SET, BACK};

    enum Mode {
      mode_oper,
      mode_oper_blk_id,
      mode_prog,
      mode_prog_blk,
      mode_prog_blk_id,
      mode_prog_blk_id_sp,
      mode_prog_blk_id_sp_edit,
      mode_prog_blk_id_hi,
      mode_prog_blk_id_hi_edit,
      mode_prog_blk_id_mo,
      mode_prog_blk_id_mo_edit,
      mode_prog_sen,
      mode_prog_sen_reset,
      mode_prog_sen_id,
      mode_prog_sys,
    };
    
    System(int ONE_WIRE_BUS, int OUT_HEAT, int OUT_COOL, int HX711_DOUT, int HX711_PD_SCK);
    void begin();    
    void Print();
    //void input_handler(int opt);
    void read_inputs();
    float temp_processing();
    float density_update();
};

#endif
