#include "Arduino.h"
#include "Sensors.h"

Sensors::Sensors(int ONE_WIRE_BUS, int HX711_DOUT, int HX711_PD_SCK):
  oneWire(ONE_WIRE_BUS),
  ds_sensor(&oneWire),
  resolution(10),
  scale(HX711_DOUT, HX711_PD_SCK)
{
}

void Sensors::begin(){
  //ds_sensor.begin();
  
  //totalDevices = DiscoverOneWireDevices();
  //for (byte i=0; i < totalDevices; i++) 
  //  ds_sensors.setResolution(allAddress[i], resolution);
}


void Sensors::requestTemperature(){
  ds_sensor.requestTemperatures();
}


float Sensors::getTemperature() {
  float tempC = ds_sensor.getTempCByIndex(0);
  return tempC;  
}


byte Sensors::checkSensor(){
  return ds_sensor.isConnected(0);
}


float Sensors::getDensity(){
  float density = scale.read_average(10);  
  return density;
}





