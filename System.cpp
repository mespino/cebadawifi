#include <Arduino.h>
#include <EEPROM.h>
#include "Control.h"
#include "Display.h"
#include "System.h"


System::System(int ONE_WIRE_BUS, int OUT_HEAT, int OUT_COOL, int HX711_DOUT, int HX711_PD_SCK):
  sensors(ONE_WIRE_BUS, HX711_DOUT, HX711_PD_SCK)
{
  pin_OUT_HEAT = OUT_HEAT;
  pin_OUT_COOL = OUT_COOL;
  pin_HX711_DOUT = HX711_DOUT;
  pin_HX711_PD_SCK = HX711_PD_SCK;
  
  current_option = 0;
  system_mode = 2;
  
  last_display_loop_time = millis();
  last_temp_processing_time = millis();
  
  System::ControlCore = new Control();
  
};


void System::begin(){
  pinMode(pin_OUT_HEAT, OUTPUT);
  pinMode(pin_OUT_COOL, OUTPUT);
  sensors.begin();    
  system_mode = mode_oper;
  Serial.print("system_mode:"); Serial.println(system_mode);
};


void System::updateStatus(){
  ControlCore->updateStatus();
}


void System::updateTemp(){
  sensors.requestTemperature();
  float tempC = sensors.getTemperature();
  if (tempC == -127.00) {
    ControlCore->setStatus(CONTROL_BLOCK_STATUS_ERROR);
  }
  else {
    ControlCore->setTemp(tempC);
  }  
}


void System::updateOutputs(){
  byte _statusControl = (byte)ControlCore->getStatus();
  if( _statusControl == CONTROL_BLOCK_STATUS_COLD_ON ){
    digitalWrite(pin_OUT_COOL, HIGH);
    digitalWrite(pin_OUT_HEAT, LOW);
  } else if( _statusControl == CONTROL_BLOCK_STATUS_HEAT_ON ){
    digitalWrite(pin_OUT_COOL, LOW);
    digitalWrite(pin_OUT_HEAT, HIGH);
  } else if( _statusControl == CONTROL_BLOCK_STATUS_OFF ){
    digitalWrite(pin_OUT_COOL, LOW);
    digitalWrite(pin_OUT_HEAT, LOW);
  }
}


float System::temp_processing(){
  //if(system_mode != mode_oper_blk_id)
  //  return -9999.0;
  updateTemp();
  updateStatus();
  updateOutputs();
  return ControlCore->getTemp();
}


float System::density_update(){
  return sensors.getDensity();   
}

